/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins.handler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import io.github.winx64.skins.cache.CachedSkin;

/**
 * Interface defining methods that execute server implementation dependent code,
 * that should be implemented separately according to the server version
 * 
 * @author WinX64
 *
 */
public interface VersionAdapter {

	/**
	 * Constructs the cache object
	 * 
	 * @param cacheLimit
	 *            The maximum number of items
	 * @param cacheExpiry
	 *            The item expiry in milliseconds
	 */
	public void constructCache(int cacheLimit, int cacheExpiry);

	/**
	 * Gets the player's internal skin property and converts it to a CachedSkin
	 * object
	 * 
	 * @param player
	 *            The player
	 * @return The skin
	 */
	public CachedSkin getPlayerSkin(Player player);

	/**
	 * Sets the player's internal skin property based on the given skin
	 * 
	 * @param player
	 *            The player
	 * @param skin
	 *            The skin
	 */
	public void setPlayerSkin(Player player, CachedSkin skin);

	/**
	 * Attempts to get the cached skin if available. If not, triggers
	 * {@link VersionAdapter#retrieveSkin(String)}
	 * 
	 * @param skinName
	 *            The skin name
	 * @return The skin
	 */
	public CachedSkin getCachedSkin(String skinName);

	/**
	 * Adds a skin to the local cache
	 * 
	 * @param skinName
	 *            The skin name
	 * @param skin
	 *            The skin
	 */
	public void cacheSkin(String skinName, CachedSkin skin);

	/**
	 * Updates the player's own entity model
	 * 
	 * @param player
	 *            The player
	 */
	public void updateOwnPlayerModel(Player player);

	/**
	 * Attempts to retrieve the player's skin information from Mojang's skin
	 * servers
	 * 
	 * @param name
	 *            The name of the player
	 * @return The skin if the player exists, or {@link CachedSkin#INVALID} if
	 *         not
	 */
	public CachedSkin retrieveSkin(String name);

	/**
	 * The API method now returns a collection rather than an array.
	 * 
	 * @return The online players
	 */
	public Collection<? extends Player> getOnlinePlayers();

	/**
	 * Reading directly from a stream is no longer possible in 1.12, and the
	 * preferred way is reading from a reader with the correct file encoding
	 * specified to maintain consistency.
	 * 
	 * @param input
	 *            The resource input stream
	 * @return The parsed yaml configuration
	 */
	public YamlConfiguration loadFromResource(InputStream input) throws IOException;
}
