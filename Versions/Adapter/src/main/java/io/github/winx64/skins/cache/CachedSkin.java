/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins.cache;

/**
 * A data class to hold the value and signature of skin properties
 * 
 * @author WinX64
 *
 */
public final class CachedSkin {

	/**
	 * A placeholder for skins that doesn't exist
	 */
	public static final CachedSkin INVALID = new CachedSkin("", "");

	private final String value;
	private final String signature;

	public CachedSkin(String value, String signature) {
		this.value = value;
		this.signature = signature;
	}

	/**
	 * Returns the skin's value
	 * 
	 * @return The value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Returns the skin's signature
	 * 
	 * @return The signature
	 */
	public String getSignature() {
		return signature;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (!(o instanceof CachedSkin)) {
			return false;
		}

		CachedSkin skin = (CachedSkin) o;
		return value.equals(skin.value) && signature.equals(skin.signature);
	}

	@Override
	public int hashCode() {
		int hash = 37;

		hash = 37 * hash + value.hashCode();
		hash = 37 * hash + signature.hashCode();

		return hash;
	}
}
