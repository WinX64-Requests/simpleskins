/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins.cache;

import com.google.common.cache.CacheLoader;

import io.github.winx64.skins.handler.VersionAdapter;

/**
 * Utility cache loader to load the skins from Mojang's servers
 * 
 * @author WinX64
 *
 */
public class SkinCacheLoader extends CacheLoader<String, CachedSkin> {

	private final VersionAdapter adapter;

	public SkinCacheLoader(VersionAdapter adapter) {
		this.adapter = adapter;
	}

	@Override
	public CachedSkin load(String name) throws Exception {
		return adapter.retrieveSkin(name);
	}
}
