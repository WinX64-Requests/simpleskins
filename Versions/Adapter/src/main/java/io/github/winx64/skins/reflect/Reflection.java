package io.github.winx64.skins.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.bukkit.Bukkit;

public final class Reflection {

	private static final String PACKAGE_VERSION;

	private static final String NMS_CLASS_PACKAGE;

	private static final String OBC_CLASS_PACKAGE;

	static {
		String[] entries = Bukkit.getServer().getClass().getPackage().getName().split("\\.");
		PACKAGE_VERSION = entries.length == 4 && entries[3].startsWith("v") ? entries[3] : null;
		NMS_CLASS_PACKAGE = "net.minecraft.server." + PACKAGE_VERSION;
		OBC_CLASS_PACKAGE = "org.bukkit.craftbukkit." + PACKAGE_VERSION;
	}

	private Reflection() {}

	public static String getPackageVersion() {
		return PACKAGE_VERSION;
	}

	public static final Class<?> getNMSClass(String className) {
		return getClass(NMS_CLASS_PACKAGE + "." + className);
	}

	public static Class<?> getOBCClass(String className) {
		return getOBCClass(null, OBC_CLASS_PACKAGE + "." + className);
	}

	public static Class<?> getOBCClass(String subPackageName, String className) {
		return getClass(OBC_CLASS_PACKAGE + (subPackageName == null ? "" : "." + subPackageName) + "." + className);
	}

	public static Field getField(Class<?> theClass, String fieldName) {
		try {
			Field theField = theClass.getDeclaredField(fieldName);
			theField.setAccessible(true);

			Field modifiers = Field.class.getDeclaredField("modifiers");
			modifiers.setAccessible(true);
			modifiers.set(theField, theField.getModifiers() & ~Modifier.FINAL);

			return theField;
		} catch (Exception ignored) {
			return null;
		}
	}

	public static Constructor<?> getConstructor(Class<?> theClass, Class<?>... parameters) {
		try {
			Constructor<?> theConstructor = theClass.getDeclaredConstructor(parameters);
			theConstructor.setAccessible(true);
			return theConstructor;
		} catch (Exception ignored) {
			return null;
		}
	}

	public static Method getMethod(Class<?> theClass, String methodName, Class<?>... parameters) {
		try {
			Method theMethod = theClass.getDeclaredMethod(methodName, parameters);
			theMethod.setAccessible(true);
			return theMethod;
		} catch (Exception ignored) {
			return null;
		}
	}

	public static Object createInstance(Constructor<?> theConstructor, Object... arguments) {
		try {
			return theConstructor.newInstance(arguments);
		} catch (Exception ignored) {
			return null;
		}
	}

	public static Object invokeMethod(Method theMethod, Object instance, Object... arguments) {
		try {
			return theMethod.invoke(instance, arguments);
		} catch (Exception ignored) {
			return null;
		}
	}

	private static Class<?> getClass(String fullClassName) {
		try {
			return Class.forName(fullClassName);
		} catch (Exception ignored) {
			return null;
		}
	}
}
