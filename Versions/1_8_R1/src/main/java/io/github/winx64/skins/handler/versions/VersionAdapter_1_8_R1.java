/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins.handler.versions;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.google.common.base.Charsets;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Iterables;
import com.mojang.authlib.Agent;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.ProfileLookupCallback;
import com.mojang.authlib.properties.Property;

import io.github.winx64.skins.cache.CachedSkin;
import io.github.winx64.skins.cache.SkinCacheLoader;
import io.github.winx64.skins.handler.VersionAdapter;
import net.minecraft.server.v1_8_R1.EntityPlayer;
import net.minecraft.server.v1_8_R1.EnumPlayerInfoAction;
import net.minecraft.server.v1_8_R1.MinecraftServer;
import net.minecraft.server.v1_8_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R1.PlayerConnection;

/**
 * Version adapter implementation for v1_8_R1
 * 
 * @author WinX64
 *
 */
public final class VersionAdapter_1_8_R1 implements VersionAdapter {

	private LoadingCache<String, CachedSkin> skinCache;

	@Override
	public void constructCache(int cacheLimit, int cacheExpiry) {
		this.skinCache = CacheBuilder.newBuilder().maximumSize(cacheLimit)
				.expireAfterWrite(cacheExpiry, TimeUnit.MILLISECONDS).build(new SkinCacheLoader(this));
	}

	@Override
	public CachedSkin getPlayerSkin(Player player) {
		GameProfile profile = ((CraftPlayer) player).getHandle().getProfile();
		Property texture = Iterables.getFirst(profile.getProperties().get("textures"), null);
		if (texture == null) {
			return CachedSkin.INVALID;
		}

		return new CachedSkin(texture.getValue(), texture.getSignature());
	}

	@Override
	public void setPlayerSkin(Player player, CachedSkin skin) {
		GameProfile profile = ((CraftPlayer) player).getHandle().getProfile();
		Property texture = new Property("textures", skin.getValue(), skin.getSignature());
		profile.getProperties().removeAll("textures");
		profile.getProperties().put("textures", texture);
	}

	@Override
	public CachedSkin getCachedSkin(String skinName) {
		try {
			return skinCache.get(skinName.toLowerCase());
		} catch (Exception ignored) {
			return CachedSkin.INVALID;
		}
	}

	@Override
	public void cacheSkin(String skinName, CachedSkin skin) {
		this.skinCache.asMap().put(skinName.toLowerCase(), skin);
	}

	@Override
	public void updateOwnPlayerModel(Player player) {
		EntityPlayer nmsPlayer = ((CraftPlayer) player).getHandle();
		PlayerConnection conn = nmsPlayer.playerConnection;
		PacketPlayOutPlayerInfo removeInfoPacket = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER,
				nmsPlayer);
		PacketPlayOutPlayerInfo addInfoPacket = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, nmsPlayer);

		conn.sendPacket(removeInfoPacket);
		conn.sendPacket(addInfoPacket);
	}

	@Override
	public CachedSkin retrieveSkin(String name) {
		final GameProfile[] result = new GameProfile[1];
		MinecraftServer.getServer().getGameProfileRepository().findProfilesByNames(new String[] { name },
				Agent.MINECRAFT, new ProfileLookupCallback() {
					@Override
					public void onProfileLookupFailed(GameProfile profile, Exception e) {}

					@Override
					public void onProfileLookupSucceeded(GameProfile profile) {
						result[0] = profile;
					}
				});

		GameProfile profile = result[0];
		if (profile == null) {
			return CachedSkin.INVALID;
		}

		MinecraftServer.getServer().aB().fillProfileProperties(profile, true);
		Property texture = Iterables.getFirst(profile.getProperties().get("textures"), null);
		if (texture == null) {
			return CachedSkin.INVALID;
		}

		return new CachedSkin(texture.getValue(), texture.getSignature());
	}

	@Override
	public Collection<? extends Player> getOnlinePlayers() {
		return Bukkit.getOnlinePlayers();
	}

	@Override
	public YamlConfiguration loadFromResource(InputStream input) throws IOException {
		try (InputStreamReader reader = new InputStreamReader(input, Charsets.UTF_8)) {
			return YamlConfiguration.loadConfiguration(reader);
		}
	}
}
