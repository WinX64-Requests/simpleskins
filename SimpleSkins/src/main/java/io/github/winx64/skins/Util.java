/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins;

import java.util.regex.Pattern;

import org.bukkit.entity.Player;

import io.github.winx64.skins.handler.VersionAdapter;

/**
 * Utility class
 * 
 * @author WinX64
 *
 */
public final class Util {

	/**
	 * Pattern of an invalid name. Valid names only contain letters, numbers and
	 * underscore(_)
	 */
	private static final Pattern INVALID_NAME_PATTERN = Pattern.compile("[^a-zA-Z0-9_]");

	private Util() {}

	/**
	 * Checks if a name is valid
	 * 
	 * @param name
	 *            The name
	 * @return If the name contains all valid and is not longer than 16
	 *         characters
	 */
	public static boolean isNameValid(String name) {
		return name != null && name.length() > 0 && name.length() <= 16 && !INVALID_NAME_PATTERN.matcher(name).find();
	}

	/**
	 * Updates the specified player model to everyone and themselves
	 * 
	 * @param player
	 *            The player
	 * @param adapter
	 *            The current version adapter
	 */
	public static void updatePlayerModel(Player player, VersionAdapter adapter) {
		for (Player target : adapter.getOnlinePlayers()) {
			if (target.canSee(player)) {
				target.hidePlayer(player);
				target.showPlayer(player);
			}
		}
		adapter.updateOwnPlayerModel(player);
	}
}
