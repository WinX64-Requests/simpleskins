/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.winx64.skins.SimpleSkins;
import io.github.winx64.skins.Util;
import io.github.winx64.skins.configuration.SkinMessages;
import io.github.winx64.skins.configuration.SkinMessages.Message;
import io.github.winx64.skins.player.SkinPlayer;

/**
 * SkinReset's command executor
 * 
 * @author WinX64
 *
 */
public final class CommandSkinReset implements CommandExecutor {

	private final SimpleSkins plugin;
	private final SkinMessages messages;

	public CommandSkinReset(SimpleSkins plugin) {
		this.plugin = plugin;
		this.messages = plugin.getSkinMessages();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(messages.get(Message.COMMAND_NO_CONSOLE));
			return true;
		}

		Player player = (Player) sender;
		SkinPlayer sPlayer = plugin.getSkinPlayer(player);
		sPlayer.setSkin(sPlayer.getOriginalSkin());
		Util.updatePlayerModel(player, plugin.getVersionAdapter());

		player.sendMessage(messages.get(Message.COMMAND_SKINREMOVE_SKIN_REMOVED));
		return true;
	}
}
