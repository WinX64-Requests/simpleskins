/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.winx64.skins.command.CommandSkin;
import io.github.winx64.skins.command.CommandSkinReload;
import io.github.winx64.skins.command.CommandSkinReset;
import io.github.winx64.skins.configuration.SkinConfiguration;
import io.github.winx64.skins.configuration.SkinMessages;
import io.github.winx64.skins.handler.VersionAdapter;
import io.github.winx64.skins.handler.VersionHandler;
import io.github.winx64.skins.listener.PlayerListener;
import io.github.winx64.skins.player.SkinPlayer;
import io.github.winx64.skins.reflect.Reflection;

/**
 * SimpleSkins' main class
 * 
 * @author WinX64
 *
 */
public final class SimpleSkins extends JavaPlugin {

	private final Map<UUID, SkinPlayer> skinPlayers;

	private final Logger logger;
	private final SkinConfiguration skinConfig;
	private final SkinMessages skinMessages;

	private VersionAdapter adapter;

	public SimpleSkins() {
		this.logger = getLogger();
		this.skinPlayers = new HashMap<>();
		this.skinConfig = new SkinConfiguration(this);
		this.skinMessages = new SkinMessages(this);
	}

	@Override
	public void onEnable() {
		String currentVersion = Reflection.getPackageVersion();
		this.adapter = VersionHandler.getAdapter(currentVersion);
		if (adapter == null) {
			logger.severe("The current server version is not supported!");
			if (currentVersion == null) {
				logger.severe("Your current version is unknown. It may be due to a severely outdated server.");
			} else if (VersionHandler.isVersionUnsupported(currentVersion)) {
				logger.severe("Your current version is " + currentVersion
						+ ". Get off your dinosaur and update your server!");
			} else {
				logger.severe("Your current version is " + currentVersion
						+ ". This is a newer version that still not supported. Ask the author to provide support for it!");
			}
			getServer().getPluginManager().disablePlugin(this);
			return;
		} else {
			logger.info("Registered version adapter with success! Using " + adapter.getClass().getSimpleName());
		}

		if (!skinConfig.loadConfiguration()) {
			logger.severe("Failed to load the configuration. The plugin will be disabled to avoid further damage!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		if (!createCache()) {

		}

		if (!skinMessages.loadMessages()) {
			logger.severe("Failed to load the messages. The plugin is unable to function correctly without them!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		for (Player player : adapter.getOnlinePlayers()) {
			SkinPlayer sPlayer = new SkinPlayer(this, player);

			skinPlayers.put(player.getUniqueId(), new SkinPlayer(this, player));
			adapter.cacheSkin(player.getName(), sPlayer.getOriginalSkin());
		}

		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new PlayerListener(this), this);

		this.getCommand("skin").setExecutor(new CommandSkin(this));
		this.getCommand("skinreset").setExecutor(new CommandSkinReset(this));
		this.getCommand("skinreload").setExecutor(new CommandSkinReload(this));
	}

	@Override
	public void onDisable() {
		if (adapter == null) {
			return;
		}
		for (SkinPlayer sPlayer : skinPlayers.values()) {
			sPlayer.setSkin(sPlayer.getOriginalSkin());
			Util.updatePlayerModel(sPlayer.getPlayer(), adapter);
		}
	}

	private boolean createCache() {
		try {
			adapter.constructCache(skinConfig.getCacheLimit(), skinConfig.getCacheExpiry());
			return true;
		} catch (Exception e) {
			log(Level.SEVERE, e, "An error occurred while trying to create the skin cache! Details below:");
			return false;
		}
	}

	public SkinConfiguration getSkinConfig() {
		return skinConfig;
	}

	public SkinMessages getSkinMessages() {
		return skinMessages;
	}

	public void log(Level level, String format, Object... objects) {
		log(level, null, format, objects);
	}

	public void log(Level level, Exception e, String format, Object... objects) {
		logger.log(level, String.format(format, objects), e);
	}

	public void registerSkinPlayer(SkinPlayer sPlayer) {
		skinPlayers.put(sPlayer.getPlayer().getUniqueId(), sPlayer);
	}

	public SkinPlayer getSkinPlayer(Player player) {
		return skinPlayers.get(player.getUniqueId());
	}

	public void unregisterSkinPlayer(SkinPlayer sPlayer) {
		skinPlayers.remove(sPlayer.getPlayer().getUniqueId());
	}

	public VersionAdapter getVersionAdapter() {
		return adapter;
	}
}
