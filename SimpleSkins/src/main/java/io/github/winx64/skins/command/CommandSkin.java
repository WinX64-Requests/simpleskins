/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.winx64.skins.SimpleSkins;
import io.github.winx64.skins.Util;
import io.github.winx64.skins.cache.CachedSkin;
import io.github.winx64.skins.configuration.SkinMessages;
import io.github.winx64.skins.configuration.SkinMessages.Message;
import io.github.winx64.skins.player.SkinPlayer;

/**
 * Skin's command executor
 * 
 * @author WinX64
 *
 */
public final class CommandSkin implements CommandExecutor {

	private final SimpleSkins plugin;
	private final SkinMessages messages;

	public CommandSkin(SimpleSkins plugin) {
		this.plugin = plugin;
		this.messages = plugin.getSkinMessages();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(messages.get(Message.COMMAND_NO_CONSOLE));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(messages.get(Message.COMMAND_SKIN_SYNTAX, alias));
			return true;
		}

		if (!Util.isNameValid(args[0])) {
			sender.sendMessage(messages.get(Message.COMMAND_SKIN_INVALID_NAME));
			return true;
		}

		Player player = (Player) sender;
		SkinPlayer sPlayer = plugin.getSkinPlayer(player);

		Bukkit.getScheduler().runTaskAsynchronously(plugin, new SkinSearch(sPlayer, args[0]));
		return true;
	}

	private final class SkinSearch implements Runnable {

		private final SkinPlayer sPlayer;
		private final String skinName;

		private SkinSearch(SkinPlayer sPlayer, String skinName) {
			this.sPlayer = sPlayer;
			this.skinName = skinName;
		}

		@Override
		public void run() {
			CachedSkin targetSkin = plugin.getVersionAdapter().getCachedSkin(skinName);
			if (targetSkin == CachedSkin.INVALID) {
				sPlayer.getPlayer().sendMessage(messages.get(Message.COMMAND_SKIN_PLAYER_NOT_FOUND, skinName));
				return;
			}

			Bukkit.getScheduler().runTask(plugin, new SkinUpdate(sPlayer, skinName, targetSkin));
		}
	}

	private final class SkinUpdate implements Runnable {

		private final SkinPlayer sPlayer;
		private final String skinName;
		private final CachedSkin skin;

		private SkinUpdate(SkinPlayer sPlayer, String skinName, CachedSkin skin) {
			this.sPlayer = sPlayer;
			this.skinName = skinName;
			this.skin = skin;
		}

		@Override
		public void run() {
			Player player = sPlayer.getPlayer();
			if (!player.isOnline()) {
				return;
			}

			sPlayer.setSkin(skin);
			Util.updatePlayerModel(player, plugin.getVersionAdapter());

			player.sendMessage(messages.get(Message.COMMAND_SKIN_SKIN_APPLIED, skinName));
		}
	}
}
