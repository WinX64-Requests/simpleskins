/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins.player;

import org.bukkit.entity.Player;

import io.github.winx64.skins.SimpleSkins;
import io.github.winx64.skins.cache.CachedSkin;

/**
 * Wrapper class for the player, that also holds some extra information
 * 
 * @author WinX64
 *
 */
public final class SkinPlayer {

	private final SimpleSkins plugin;
	private final Player player;

	private final CachedSkin originalSkin;

	public SkinPlayer(SimpleSkins plugin, Player player) {
		this.plugin = plugin;
		this.player = player;

		this.originalSkin = getSkin();
	}

	/**
	 * Returns the Player instance linked to this FakePlayer
	 * 
	 * @return The Player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Returns the player's original skin
	 * 
	 * @return The skin
	 */
	public CachedSkin getOriginalSkin() {
		return originalSkin;
	}

	/**
	 * Returns the player's current skin
	 * 
	 * @return The skin
	 */
	public CachedSkin getSkin() {
		return plugin.getVersionAdapter().getPlayerSkin(player);
	}

	/**
	 * Sets the player's skin
	 * 
	 * @param skin
	 *            The skin
	 */
	public void setSkin(CachedSkin skin) {
		this.plugin.getVersionAdapter().setPlayerSkin(player, skin);
	}
}
