/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import io.github.winx64.skins.SimpleSkins;
import io.github.winx64.skins.configuration.SkinMessages;
import io.github.winx64.skins.configuration.SkinMessages.Message;

/**
 * SkinReload's command executor
 * 
 * @author WinX64
 *
 */
public final class CommandSkinReload implements CommandExecutor {

	private final SimpleSkins plugin;
	private final SkinMessages messages;

	public CommandSkinReload(SimpleSkins plugin) {
		this.plugin = plugin;
		this.messages = plugin.getSkinMessages();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (plugin.getSkinMessages().loadMessages()) {
			sender.sendMessage(messages.get(Message.COMMAND_RELOAD_SUCCESS));
		} else {
			sender.sendMessage(messages.get(Message.COMMAND_RELOAD_FAILURE));
		}
		return true;
	}
}
