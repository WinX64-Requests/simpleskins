/*
 *   SimpleSkins - A simple plugin for skin handling
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.skins.listener;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import io.github.winx64.skins.SimpleSkins;
import io.github.winx64.skins.cache.CachedSkin;
import io.github.winx64.skins.player.SkinPlayer;

/**
 * Player event listeners
 * 
 * @author WinX64
 *
 */
public final class PlayerListener implements Listener {

	private final SimpleSkins plugin;

	private final Map<String, CachedSkin> pendingSkins;

	public PlayerListener(SimpleSkins plugin) {
		this.plugin = plugin;

		this.pendingSkins = new ConcurrentHashMap<String, CachedSkin>();
	}

	@EventHandler
	public void onPreLogin(AsyncPlayerPreLoginEvent event) {
		if (event.getLoginResult() != AsyncPlayerPreLoginEvent.Result.ALLOWED) {
			return;
		}

		if (!Bukkit.getOnlineMode()) {
			CachedSkin pendingSkin = plugin.getVersionAdapter().getCachedSkin(event.getName());
			if (pendingSkin != CachedSkin.INVALID) {
				this.pendingSkins.put(event.getName(), pendingSkin);
			}
		}
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent event) {
		if (event.getResult() != PlayerLoginEvent.Result.ALLOWED) {
			return;
		}

		if (!Bukkit.getOnlineMode()) {
			Player player = event.getPlayer();
			CachedSkin pendingSkin = pendingSkins.remove(player.getName());
			if (pendingSkin != null && pendingSkin != CachedSkin.INVALID) {
				plugin.getVersionAdapter().setPlayerSkin(player, pendingSkin);
			}
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		SkinPlayer sPlayer = new SkinPlayer(plugin, player);
		plugin.registerSkinPlayer(sPlayer);
		plugin.getVersionAdapter().cacheSkin(player.getName(), sPlayer.getOriginalSkin());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		SkinPlayer sPlayer = plugin.getSkinPlayer(player);

		if (sPlayer != null) {
			plugin.unregisterSkinPlayer(sPlayer);
		}
	}
}
